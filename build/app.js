(function(){
	'use strict';

	angular.module('tr', ['ui.router','tr-main','tr-services','templates', 'ngAnimate'])
	  .config(function ($stateProvider, $urlRouterProvider) {
	    $urlRouterProvider
	      .otherwise("/");
	  });
	  
})();
'app controller goes here';
(function(){
	'use strict';

	angular.module('tr-services', [])
		.factory('httpservice', ['$http', function($http){
			return{
				getHotels: function(callback, errback){
	  				$http.get("hotels.json")
	  				.success(callback)
	  				.error(errback);
	  			}
			};
		}]);
})();
(function(){
  'use strict';


  angular.module('tr-main',['ui.router'])
    .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
      $stateProvider
        .state('main', {
          url: '/',
          templateUrl: 'main/main.html',
          controller: 'MainCtrl'
        });
        $locationProvider.html5Mode(true);
    })
    .controller('MainCtrl', function ($scope, httpservice) {
      $scope.predicate = 'UserRating';
      $scope.reverse = true;
      $scope.order = function(predicate) {
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.predicate = predicate;
      };
      httpservice.getHotels(function(data){
          $scope.hotels = data.Establishments;
      },
      function(){
        console.log("error");
      });

    });

})();