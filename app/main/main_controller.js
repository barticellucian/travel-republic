(function(){
  'use strict';


  angular.module('tr-main',['ui.router'])
    .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
      $stateProvider
        .state('main', {
          url: '/',
          templateUrl: 'main/main.html',
          controller: 'MainCtrl'
        });
        $locationProvider.html5Mode(true);
    })
    .controller('MainCtrl', function ($scope, httpservice) {
      $scope.predicate = 'UserRating';
      $scope.reverse = true;
      $scope.order = function(predicate) {
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.predicate = predicate;
      };
      httpservice.getHotels(function(data){
          $scope.hotels = data.Establishments;
      },
      function(){
        console.log("error");
      });

    });

})();