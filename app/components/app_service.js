(function(){
	'use strict';

	angular.module('tr-services', [])
		.factory('httpservice', ['$http', function($http){
			return{
				getHotels: function(callback, errback){
	  				$http.get("hotels.json")
	  				.success(callback)
	  				.error(errback);
	  			}
			};
		}]);
})();