(function(){
	'use strict';

	angular.module('tr', ['ui.router','tr-main','tr-services','templates', 'ngAnimate'])
	  .config(function ($stateProvider, $urlRouterProvider) {
	    $urlRouterProvider
	      .otherwise("/");
	  });
	  
})();